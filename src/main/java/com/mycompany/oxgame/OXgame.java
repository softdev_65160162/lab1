/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.oxgame;

/**
 *
 * @author Asus
 */
import java.util.*;
public class OXgame {
    static String[] board;
    static String turn;
    static String check(){
        for (int a = 0; a < 8; a++){
            String line = null;
            switch (a){
                case 0:
                    line = board[0] + board[1] + board[2];
                    break;
                case 1:
                    line = board[3] + board[4] + board[5];
                    break;
                case 2:
                    line = board[6] + board[7] + board[8];
                    break;
		case 3:
                    line = board[0] + board[3] + board[6];
                    break;
                case 4:
                    line = board[1] + board[4] + board[7];
                    break;
		case 5:
                    line = board[2] + board[5] + board[8];
                    break;
		case 6:
                    line = board[0] + board[4] + board[8];
                    break;
		case 7:
                    line = board[2] + board[4] + board[6];
                    break;
            }
            
            if(line.equals("XXX")){
                return "X";
            }else if(line.equals("OOO")){
                return "O";
            }
            
        }
        
        for(int a = 0; a < 9; a++){
            if(Arrays.asList(board).contains(String.valueOf(a + 1))){
                break;
            }else if(a == 8){
                return "draw";
            }
        }
        
        return null ;
    }
    
    static void printtable(){
        System.out.println(board[0]+" "+board[1]+" "+board[2]);
        System.out.println(board[3]+" "+board[4]+" "+board[5]);
        System.out.println(board[6]+" "+board[7]+" "+board[8]);
    }
    
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        board = new String[9];
        turn = "O";
        String winner = null;
        
        for(int a = 0; a < 9; a++){
            board[a] = String.valueOf(a + 1);
        }
        System.out.println("Welcome OX");
        printtable();
        
        while(winner==null){
            System.out.println(turn+" turn");
            System.out.print("Please input number:");
            
            int num = kb.nextInt();
            if(board[num - 1].equals(String.valueOf(num))){
                board[num - 1] = turn;
                if(turn.equals("X")){
                    printtable();
                    turn = "O";
                }else{
                    printtable();
                    turn = "X";
                }
                winner = check();
            }
        }
        if(winner.equalsIgnoreCase("draw")){
            System.out.println("draw");
        }else{
            System.out.println(winner+" win");
        }
    }
}
